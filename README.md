# DIP Web React Client App

## Install App Dependencies

```
npm i
```

## Build App to create ./dist folder

```
npm run build
```

### for production

```
npm run build:prod
```

## Start App and test

```
npm run start
```

### for production

```
npm run start:prod
```