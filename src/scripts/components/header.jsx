import React from 'react';

const Header = () => {
    return (
        <nav className="navbar navbar-default tm-nav header-component">
            <div className="container">
                <h2>
                    <a href="#" className="navbar-brand">DIP-CV-WEB</a>
                </h2>
            </div>
        </nav>
    );
};

export default Header;