import React from 'react';

const Footer = () => {
    return (
        <footer className="tm-footer footer-component">
            <div className="container">
                <p>Dip CV Web Client © 2016</p>
            </div>
        </footer>
    );
};

export default Footer;