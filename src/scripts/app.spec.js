/*eslint-disable*/
import App from './app';
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';

describe("components - Trafic Meister App - ", function() {
    let component;

    beforeEach(function() {
        /**
         * render component on the element created
         */
        component = ReactTestUtils.renderIntoDocument(<App />);
    });

    it('can render App root component without error', function() {
        // Render into a document fragment and return the full component instance.
        expect(component).toBeDefined();
    });
});

/*eslint-enable*/
