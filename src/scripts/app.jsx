import React from 'react';
import Header from './components/header';
import Footer from './components/footer';

const App = () => {
  return (
    <div>
      <Header />
        <div className="container">
          <h1>APP CONTENT GOES HERE</h1>
        </div>
      <Footer />
    </div>
  );
}

export default App;